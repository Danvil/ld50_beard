using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Ikabur.LD50_Beard {
  public class BeardScoreGui : MonoBehaviour {
    public BeardMeasure beardMeasure;

    public TextMeshProUGUI text;

    public void SetScores(int[] stars) {
      text.text = string.Format(
          "Beard: {0}/5\n" +
          "Mustache: {1}/5\n" +
          "Eyebrows: {2}/5\n" +
          "Nose Hair: {3}/5\n" +
          "Ear Hair: {4}/5\n",
          stars[0],
          stars[1],
          stars[2],
          stars[3],
          stars[4]);
    }

    void Start() {

    }

    void Update() {
      SetScores(beardMeasure.Stars);
    }
  }
}
