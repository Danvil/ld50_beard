using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  public class BeardMeasure : MonoBehaviour {
    public BeardCutLevels beardCutLevels;

    public Beardscape beardscape;

    public BeardParts parts;

    public AnimationCurve sensitivity;

    public float maxStrandLength;

    public float outlierThreshold;

    public TextMeshProUGUI debugText;

    Level level;

    struct StrandProps {
      public float targetLength;
      public int partIndex;
      public float sensitivity;
    }

    struct PartStats {
      public float targetLength;
      public float averageLength;
      public float deviation;
      public float direction;
      public float outlierCount;
      public float count;

      public override string ToString() {
        return string.Format("{0:0.00}/{1:0.00}; dev={2:0.00}, dir={3:0.00}, out={4:0.00}",
          targetLength, averageLength, deviation, direction, outlierCount);
      }
    }

    List<StrandProps> strandProps;

    float[] partCount;

    float[] scores;
    public float[] Scores { get { return scores; } }

    int[] stars;
    public int[] Stars { get { return stars; } }

    void Start() {
      scores = new float[5] { 3f, 3f, 3f, 3f, 3f };
      stars = new int[5] { 3, 3, 3, 3, 3 };
      level = null;
      InvokeRepeating("Judge", 1.0f, 0.25f);
    }

    void Update() {

    }

    public void SetLevel(Level level) {
      this.level = level;
      ComputeProps();
    }

    public float scoreDeviationMult = 2f;
    public float scoreDeviationSink = 0.25f;

    void Judge() {
      if (level == null) {
        return;
      }

      PartStats[] stats = Evaluate();

      // scores
      for (int i = 0; i < stats.Length; i++) {
        scores[i] = Mathf.Max(1f, 5f - scoreDeviationMult * Mathf.Max(0f, stats[i].deviation - scoreDeviationSink));
        stars[i] = Mathf.RoundToInt(scores[i]);
      }

      // debug
      string dbg = "";
      for (int i = 0; i < stats.Length; i++) {
        dbg += string.Format("{0}={{{1}}}\n", parts.partNames[i], stats[i].ToString());
      }
      debugText.text = dbg;
    }

    void ComputeProps() {
      strandProps = new List<StrandProps>(beardscape.Strands.Count);
      partCount = new float[parts.PartCount];

      var cols = level.map.GetPixels32();
      int w = level.map.width;

      foreach (var s in beardscape.Strands) {
        Vector2Int px = s.mapPxPos;
        int i = px.x;
        int j = px.y;
        var col = cols[j * w + i];
        float len = beardCutLevels.LevelColorToTargetLength(col);
        int partIdx = parts.GetPartIndex(i, j);

        strandProps.Add(new StrandProps() {
          targetLength = len,
          partIndex = partIdx,
          sensitivity = sensitivity.Evaluate(len)
        });

        partCount[partIdx] += 1f;
      }
    }

    PartStats[] Evaluate() {
      PartStats[] partStats = new PartStats[parts.PartCount];

      var strands = beardscape.Strands;
      for (int i = 0; i < strandProps.Count; i++) {
        StrandProps props = strandProps[i];
        float actual = strands[i].Strand.TotalLength;
        float serror = (actual - props.targetLength) / props.sensitivity;
        partStats[props.partIndex].targetLength += props.targetLength;
        partStats[props.partIndex].averageLength += actual;
        partStats[props.partIndex].deviation += Mathf.Abs(serror);
        partStats[props.partIndex].direction += serror;
        if (serror > outlierThreshold) {
          partStats[props.partIndex].outlierCount += 1f;
        }
        partStats[props.partIndex].count += 1f;
      }

      for (int i = 0; i < partStats.Length; i++) {
        partStats[i].targetLength /= partStats[i].count;
        partStats[i].averageLength /= partStats[i].count;
        partStats[i].deviation /= partStats[i].count;
        partStats[i].direction /= partStats[i].count;
      }

      return partStats;
    }
  }
}
