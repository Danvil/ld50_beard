using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RazorBlade : MonoBehaviour {
  public Transform blade;

  public float movementSpan = 0.1f;
  public float movementSpeed = 1.0f;

  public AudioSource audioSource;

  bool isCutting;

  float bladePosition;
  float bladeMoveDirection;

  public bool IsCutting { get { return isCutting; } }

  void Start() {
    bladePosition = 0f;
    bladeMoveDirection = 1f;
  }

  void Update() {
    if (isCutting) {
      bladePosition += bladeMoveDirection * Time.deltaTime * movementSpeed;
      if (Mathf.Abs(bladePosition) > movementSpan) {
        bladePosition = bladeMoveDirection * movementSpan;
        bladeMoveDirection *= -1f;
      }
    }
    Vector3 bladePos3 = blade.transform.localPosition;
    bladePos3.z = bladePosition;
    blade.transform.localPosition = bladePos3;
  }

  private void OnEnable() {
    SwitchOff();
  }

  private void OnDisable() {
    SwitchOff();
  }

  public void SwitchOn() {
    audioSource.Play();
    isCutting = true;
  }

  public void SwitchOff() {
    audioSource.Pause();
    isCutting = false;
  }

  public void Toggle() {
    if (isCutting) {
      SwitchOff();
    } else {
      SwitchOn();
    }
  }
}
