﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace PGA {
  public static class PGA3DHelpers {
    /// <summary>
    /// Create PGA point from Unity 3D point
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public static PGA3D ToPgaPoint(this Vector3 p) {
      return PGA3D.point(p.x, p.y, p.z);
    }

    public static PGA3D transform(this PGA3D m, PGA3D x) {
      return m * x * ~m;
    }

    /// <summary>
    /// Translate point 'p1' by distance delta towards point 'p2'
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="delta"></param>
    /// <returns></returns>
    public static PGA3D TranslateTowards(this PGA3D p1, PGA3D p2, float delta) {
      //return transform(PGA3D.translator(delta, PGA3D.e0 * (p2 & p1).normalized() * !PGA3D.e0), p1);
      return transform(PGA3D.translator(delta,
          mul_x_e123(mul_e0_x((p2 & p1).normalized()))
        ), p1);
    }
    
    static PGA3D mul_e0_x(PGA3D b)
    {
      PGA3D res = new PGA3D();
      res[0]=0;
      res[1]= +b[0];
      res[2]=0;
      res[3]=0;
      res[4]=0;
      res[5]= +b[2];
      res[6]= +b[3];
      res[7]= +b[4];
      res[8]=0;
      res[9]=0;
      res[10]=0;
      res[11]= -b[8];
      res[12]= -b[9];
      res[13]= -b[10];
      res[14]=0;
      res[15]= +b[14];
      return res;
    }

    static PGA3D mul_x_e123(PGA3D a)
    {
      PGA3D res = new PGA3D();
      res[0] = -a[14];
      res[1] = -a[15];
      res[2] = -a[10];
      res[3] = -a[9];
      res[4] = -a[8];
      res[5] = +a[13];
      res[6] = +a[12];
      res[7] = +a[11];
      res[8] = +a[4];
      res[9] = +a[3];
      res[10] = +a[2];
      res[11] = -a[7];
      res[12] = -a[6];
      res[13] = -a[5];
      res[14] = +a[0];
      res[15] = +a[1];
      return res;
    }

    /// <summary>
    /// Computes distance between two points
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    public static float DistanceTo(this PGA3D p1, PGA3D p2) {
      //return (p1.normalized() & p2.normalized()).norm();
      return (p1 & p2).norm() / (p1.norm() * p2.norm());
    }

    /// <summary>
    /// Converts a point PGA object to a unit vector
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public static Vector3 ToUnityPoint(this PGA3D p) {
      return new Vector3(p[PGA3D.kE032], p[PGA3D.kE013], p[PGA3D.kE021]);
    }
  }
}
