using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LooseStrandManager : MonoBehaviour {
  public float destroyYLimit = -10f;

  public int maxCount = 100;

  List<Ikabur.LD50_Beard.StrandBehaviour> strands;

  void Start() {
    strands = new List<Ikabur.LD50_Beard.StrandBehaviour>();
  }

  void Update() {
    var rmv = new List<Ikabur.LD50_Beard.StrandBehaviour>();

    foreach (var s in strands) {
      s.GrowAndUpdate();

      if (s.Strand.Tip.y < destroyYLimit) {
        rmv.Add(s);
      }
    }

    foreach(var s in rmv) {
      GameObject.Destroy(s.gameObject);
      strands.Remove(s);
    }

    // trim
    if (strands.Count > maxCount) {
      int n = strands.Count - maxCount;
      for (int i = 0; i < n; i++) {
        GameObject.Destroy(strands[i].gameObject);
      }
      strands.RemoveRange(0, n);
    }
  }

  public void Add(Ikabur.LD50_Beard.StrandBehaviour strand) {
    strands.Add(strand);
    strand.gameObject.transform.parent = transform;
  }
}
