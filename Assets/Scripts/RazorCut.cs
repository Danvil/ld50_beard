using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RazorCut : MonoBehaviour {
  public RazorBlade blade;

  public Transform tip;

  public float radius;

  public Beardscape beardscape;

  public LooseStrandManager loose;

  public float cutLength;

  public float strandDropProp = 0.5f;

  public BoxCollider cutbox;

  void Start() {

  }

  void Update() {
    if (!blade.IsCutting) {
      return;
    }

    foreach (var s in beardscape.Strands) {
      if (cutbox) { 
        Vector3 root = s.Strand.Root;
        root.z = tip.position.z;
        if (!cutbox.bounds.Contains(root)) { continue; }
      } else { 
        Vector3 delta = s.Strand.Root - tip.position;
        delta.z = 0f;
        if (delta.magnitude > radius) { continue; }
      }

      var cutoff = s.Cut(cutLength);
      if (cutoff != null) {
        if (Random.Range(0f, 1f) < strandDropProp) {
          var clone = GameObject.Instantiate(s);
          clone.Strand = cutoff;
          clone.isGrowing = false;
          clone.RecoverBonesFromTransform();
          loose.Add(clone);
        }
      }
    }
  }

  private void OnDrawGizmos() {
    Gizmos.DrawWireSphere(tip.position, radius);
  }
}
