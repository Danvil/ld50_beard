﻿using System.Collections.Generic;
using System.Linq;
using PGA;
using UnityEngine;

//using PointT = PGA.PGA3D;
using PointT = UnityEngine.Vector3;

namespace Ikabur.LD50_Beard {
  [System.Serializable]
  public struct StrandOptions {
    public int segmentCount;
    public float growthSpeed;
    public float segmentLength;
    public float width;

    public float strandMass;
    public float springFactor;
    public float stiffness;
    public float dampenFactor;

    public float minCut;
  }

  public struct BoneTransform {
    public Quaternion q;  // rotation
    public Vector3 p;  // position
    public Vector3 s;  // scale
  }

  public class Strand {
    private StrandOptions options;

    private PointT[] points;
    private Vector3[] forces;
    private Vector3[] velocities;

    private BoneTransform[] bones;

    private int tipIndex;
    private float tipSegmentLength;
    private float totalLength;

    public bool isRootStatic = true;

    public Strand(StrandOptions options) {
      this.options = options;

      int numPoints = options.segmentCount + 1;  // 2 segments have 3 points including start
      points = Enumerable.Repeat(Vector3.zero, numPoints).ToArray();
      forces = Enumerable.Repeat(Vector3.zero, numPoints).ToArray();
      velocities = Enumerable.Repeat(Vector3.zero, numPoints).ToArray();

      bones = new BoneTransform[options.segmentCount];
    }

    public int SegmentCount { get { return Mathf.Max(points.Length - 1, 0); } }

    public float TotalLength { get { return totalLength; } }

    public Vector3 Root {
      get { return ToVec3(points[0]); }
      set { points[0] = value; }
    }

    public Vector3 Tip {
      get { return ToVec3(points[Mathf.Min(tipIndex, points.Length - 1)]); }
    }

    #region math

    // private PGA3D[] points;

    // PGA3D FromVec3(Vector3 v) {
    //   return v.ToPgaPoint();
    // }

    // Vector3 ToVec3(PGA3D v) {
    //   return v.ToUnityPoint();
    // }

    // float Distance(PGA3D a, PGA3D b) {
    //   return a.DistanceTo(b);
    // }

    // PGA3D TranslateTowards(PGA3D source, PGA3D target, float delta) {
    //   return source.TranslateTowards(target, delta);
    // }

    //PGA3D OffsetInZ(PGA3D p, float z) {
    //  return p - z * PGA3D.e021;
    //}

    Vector3 FromVec3(Vector3 v) {
      return v;
    }

    Vector3 ToVec3(Vector3 v) {
      return v;
    }

    float Distance(Vector3 a, Vector3 b) {
      return (a - b).magnitude;
    }

    Vector3 TranslateTowards(Vector3 source, Vector3 target, float delta) {
      return source + (target - source).normalized * delta;
    }

    Vector3 OffsetInZ(Vector3 p, float z) {
      return new Vector3(p.x, p.y, p.z + z);
    }

    #endregion


    public void StartGrowth(Vector3 start) {
      points[0] = FromVec3(start);
      for (int i = 1; i < points.Length; i++) {
        points[i] = points[0];
      }

      tipIndex = 1;
      tipSegmentLength = 0.0f;
      totalLength = 0.0f;
    }

    public void ResetLength(float len) {
      tipIndex = -1;
      totalLength = len;
      for (int i = 1; i < points.Length; i++) {
        if (len <= options.segmentLength) {
          if (tipIndex == -1) {
            tipIndex = i;
            points[i] = points[i - 1] + new Vector3(0f, 0f, len);
            tipSegmentLength = len;
          } else {
            points[i] = points[tipIndex];
          }
        } else {
          points[i] = points[i - 1] + new Vector3(0f, 0f, options.segmentLength);
          len -= options.segmentLength;
        }
      }
      if (tipIndex == -1) {
        tipIndex = points.Length;
      }
    }

    public void GrowTowards(float dt, Vector3 target) {
      if (points == null || tipIndex >= points.Length) { return; }

      // distances
      float delta = dt * options.growthSpeed;
      bool expand = false;
      if (tipSegmentLength + delta > options.segmentLength) {
        delta = options.segmentLength - tipSegmentLength;
        expand = true;
      }

      tipSegmentLength += delta;
      totalLength += delta;

      // translate last point away from previous point
      points[tipIndex] = TranslateTowards(points[tipIndex], FromVec3(target), delta);

      // make sure that ungrown points make sense
      for (int i = tipIndex + 1; i < points.Length; i++) {
        points[i] = points[tipIndex];
      }

      // create new segment if grown enough
      if (expand) {
        tipIndex++;
        tipSegmentLength = 0f;
      }
    }

    public void ApplyPhysics(float dt, int steps) {
      Vector3 gravity = new Vector3(0f, -9.81f, 0f);

      int startIndex = isRootStatic ? 1 : 0;
      int endIndex = Mathf.Min(tipIndex + 1, points.Length);

      float sdt = dt / steps;
      float smass = options.strandMass / options.segmentCount;

      for (int k = 0; k < steps; k++) {
        // compute forces
        for (int i = startIndex; i < endIndex; i++) {
          // 1) gravity
          forces[i] = smass * gravity;

          // 2) segment springs
          if (i > 0) {
            Vector3 delta = points[i] - points[i - 1];
            float targetLen = (i == tipIndex ? tipSegmentLength : options.segmentLength);
            float scale = options.springFactor * (delta.magnitude - targetLen);
            forces[i] -= scale * delta;
            forces[i - 1] += scale * delta;
          }

          // 3) stiffness
          if (i > 0) {
            Vector3 tdir = (i == 1) ? Vector3.forward : points[i - 1] - points[i - 2];
            Vector3 cdir = points[i] - points[i - 1];
            float tdirLen = tdir.magnitude;
            float cdirLen = cdir.magnitude;
            if (tdirLen > 0.01 && cdirLen > 0.01) {
              Vector3 omega = Vector3.Cross(cdir, tdir) / (tdirLen * cdirLen);
              Vector3 F = Vector3.Cross(omega, cdir) / cdirLen;
              forces[i] += F * options.stiffness;
            }
          }

          // 4) dampen
          forces[i] -= options.dampenFactor * velocities[i];
        }

        // integrate
        for (int i = startIndex; i < endIndex; i++) {
          velocities[i] += forces[i] * sdt / smass;
          points[i] += velocities[i] * sdt;
        }
      }

      for (int i = endIndex; i < points.Length; i++) {
        points[i] = points[tipIndex];
      }
    }

    /// <summary>
    /// Cuts strand at given length and returns a new strand with the cutoff
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    public Strand Cut(float length) {
      if (totalLength + options.minCut <= length) {
        return null;
      }

      //                   V          T
      //   0-----1-----2-----3-----4--5
      //               ^
      //                \-- cutIndex
      //
      //   NEW:                       T
      //                     0-----1--2
      //
      //   OLD:            T 
      //   0-----1-----2---3

      int cutIndex = Mathf.FloorToInt(length / options.segmentLength);
      cutIndex = Mathf.Clamp(cutIndex, 0, points.Length - 2);

      // create new strand with points after
      Strand strand = new Strand(options);
      for (int i = cutIndex + 1; i < points.Length; i++) {
        strand.points[i - (cutIndex + 1)] = points[i];
      }
      strand.tipIndex = Mathf.Max(1, this.tipIndex - (cutIndex + 1));
      strand.tipSegmentLength = this.tipSegmentLength;
      strand.totalLength = this.totalLength - options.segmentLength * (cutIndex + 1);
      strand.isRootStatic = false;

      // trim back current strand
      this.tipIndex = cutIndex + 1;
      this.tipSegmentLength = length - cutIndex * options.segmentLength;
      this.totalLength = length;
      this.isRootStatic = true;
      this.points[tipIndex] = this.points[tipIndex - 1] + this.tipSegmentLength * new Vector3(0f, 0f, 1f);

      return strand;
    }

    public BoneTransform[] UpdateAndGetBones() {
      int end = Mathf.Min(tipIndex, points.Length - 1);
      for (int i = 0; i < end; i++) {
        // compute rotation

        // Assume three points: p0 - p1 - p2
        PointT p0, p1, p2;
        p1 = points[i];
        p2 = points[i + 1];
        if (i == 0) {
          p0 = OffsetInZ(p1, -1f);  // offset in -Z direction
        } else {
          p0 = points[i - 1];
        }

        // Connecting lines: p0 & p1 and p1 & p2
        // PGA3D r = (p0 & p1) & (p1 & p2);

        Vector3 fromDir = ToVec3(p1) - ToVec3(p0);
        Vector3 toDir = ToVec3(p2) - ToVec3(p1);
        Quaternion rot;
        if (fromDir.sqrMagnitude < 0.001 || toDir.sqrMagnitude < 0.001) {
          rot = Quaternion.identity;
        } else {
          rot = Quaternion.FromToRotation(fromDir, toDir);
        }

        float scl = Mathf.Min(1f, Distance(p1, p2) / options.segmentLength);
        Vector3 pos = i == 0
          ? ToVec3(p1)  // first bone is positioned at root
          : (options.segmentLength * Vector3.forward);  // others are "real" bones

        // create bone transform
        bones[i] = new BoneTransform() {
          q = rot,
          p = pos,
          s = new Vector3(1f, 1f, scl)
        };
      }
      for (int i = end; i < points.Length - 1; i++) {
        bones[i] = new BoneTransform() {
          q = Quaternion.identity,
          p = Vector3.zero,
          s = new Vector3(0f, 0f, 0f)
        };
      }
      return bones;
    }

    static public Mesh CreateMesh(StrandOptions options) {
      // create a segmented tube
      int numSegs = options.segmentCount;
      Debug.Assert(numSegs >= 2);
      int numCross = numSegs + 1;  // 2 segments have 3 cross sections
      int vertexCount = numCross * 4;  // 4 points per cross section
      int quadCount = numSegs * 4 + 2;  // 4 quads per segment + 2 caps
      int indexCount = 6 * quadCount;  // 2 tris per quad and 3 indices per tri

      // vertices
      //
      // Y ^
      //   |  3---2
      //   |  |   |
      //   |  0---1
      //   +--------> X
      Vector3[] vertices = new Vector3[vertexCount];
      float extends = 0.5f * options.width;
      for (int i = 0; i < numCross; i++) {
        float z = i * options.segmentLength;
        for (int j = 0; j < 4; j++) {
          float x = (j == 0 || j == 3) ? -1.0f : +1.0f;
          float y = (j == 0 || j == 1) ? -1.0f : +1.0f;
          vertices[i * 4 + j] = new Vector3(extends * x, extends * y, z);
        }
      }

      // triangle indices
      //
      //     7---6
      //     |   |\
      //     4---5 \
      //      \   \ 2
      //       \   \|
      //        0---1
      //
      int[] indices = new int[indexCount];
      for (int i = 0, k = 0; i < numSegs; i++) {
        for (int j = 0; j < 4; j++, k += 6) {
          int u = j;
          int v = (j + 1) % 4;
          int q1 = i * 4 + u;
          int q2 = i * 4 + v;
          int q3 = q2 + 4;
          int q4 = q1 + 4;
          indices[k + 0] = q1;
          indices[k + 1] = q2;
          indices[k + 2] = q3;
          indices[k + 3] = q1;
          indices[k + 4] = q3;
          indices[k + 5] = q4;
        }
      }
      // start cap
      indices[6 * quadCount - 12 + 0] = 0;
      indices[6 * quadCount - 12 + 1] = 2;
      indices[6 * quadCount - 12 + 2] = 1;
      indices[6 * quadCount - 12 + 3] = 0;
      indices[6 * quadCount - 12 + 4] = 3;
      indices[6 * quadCount - 12 + 5] = 2;
      // end cap
      indices[6 * quadCount - 6 + 0] = vertexCount - 4;
      indices[6 * quadCount - 6 + 1] = vertexCount - 3;
      indices[6 * quadCount - 6 + 2] = vertexCount - 2;
      indices[6 * quadCount - 6 + 3] = vertexCount - 4;
      indices[6 * quadCount - 6 + 4] = vertexCount - 2;
      indices[6 * quadCount - 6 + 5] = vertexCount - 1;

      // uv coordinates
      // Use relative strand length as u coordinate.
      // v coordinate is alwasy 0
      Vector2[] uvs = new Vector2[vertexCount];
      for (int i = 0; i < numCross; i++) {
        float u = i / (float)(numCross - 1);
        for (int j = 0; j < 4; j++) {
          uvs[i * 4 + j] = new Vector2(u, 0.0f);
        }
      }

      Mesh mesh = new Mesh();
      mesh.vertices = vertices;
      mesh.triangles = indices;
      mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt16;
      mesh.uv = uvs;

      // bone vertices
      // first cross section moves with first bones
      // cross section i moves with bone i - 1
      // one bone per cross section
      var bonesPerVertex = new Unity.Collections.NativeArray<byte>(vertexCount, Unity.Collections.Allocator.Temp);
      for (int i = 0; i < vertexCount; i++) {
        bonesPerVertex[i] = 1;
      }
      var weights = new Unity.Collections.NativeArray<BoneWeight1>(vertexCount, Unity.Collections.Allocator.Temp);
      for (int i = 0; i < vertexCount; i++) {
        int bone = Mathf.Max(i / 4 - 1, 0);
        weights[i] = new BoneWeight1() { boneIndex = bone, weight = 1.0f };
      }
      mesh.SetBoneWeights(bonesPerVertex, weights);

      // bind poses
      Matrix4x4[] bindposes = new Matrix4x4[numSegs];
      for (int i = 0; i < bindposes.Length; i++) {
        float z = i * options.segmentLength;
        bindposes[i] = Matrix4x4.Translate(-z * Vector3.forward);
      }
      mesh.bindposes = bindposes;

      mesh.RecalculateBounds();
      mesh.RecalculateNormals();
      mesh.RecalculateTangents();

      return mesh;
    }
  }
}
