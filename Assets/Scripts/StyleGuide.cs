﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  public class StyleGuide : MonoBehaviour {
    public BeardCutLevels beardCutLevels;

    public Texture2D chad;
    public Texture2D growth;

    public UnityEngine.UI.RawImage styleGuideImage;

    public void SetLevel(Level level) {
      styleGuideImage.texture = CreateStyleGuide(level);
    }

    Texture2D CreateStyleGuide(Level level) {
      var chadCols = chad.GetPixels();
      var growthCols = growth.GetPixels();
      var levelCols = level.map.GetPixels();

      int w = level.map.width;
      int h = level.map.height;

      var guide = new Color[levelCols.Length];

      for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
          int k = j * w + i;
          Vector3 chadCol = BeardCutLevels.ColorToVec3(chadCols[k]);
          Vector3 growthCol = BeardCutLevels.ColorToVec3(growthCols[k]);
          Vector3 levelCol = BeardCutLevels.ColorToVec3(
            beardCutLevels.LevelColorToVisColor(levelCols[k]));

          float mix = (1f - growthCol.y);
          Vector3 col = mix * levelCol + (1f - mix) * chadCol;

          guide[k] = new Color(col.x, col.y, col.z, chadCols[k].a);
        }
      }

      Texture2D tex = new Texture2D(w, h, chad.format, false);
      //tex.alphaIsTransparency = chad.alphaIsTransparency;
      tex.filterMode = chad.filterMode;
      tex.SetPixels(guide);
      tex.Apply();
      return tex;
    }
  }
}
