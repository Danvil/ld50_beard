using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeardscapeMap {
  float planeSize;
  int width, height;

  float Sx, Sy;
  float Dx, Dy;

  public BeardscapeMap(float planeSize, int width, int height) {
    this.planeSize = planeSize;
    this.width = width;
    this.height = height;

    Sx = -planeSize / width;
    Dx = 0.5f * planeSize - 0.5f * Sx;

    Sy = planeSize / height;
    Dy = -0.5f * planeSize + 0.5f * Sy;
  }

  public Vector2 MapPixelToWorld(int i, int j) {
    return new Vector2(
      i * Sx + Dx,
      j * Sy + Dy);
  }

  public Vector2Int WorldToMapPixel(Vector2 u) {
    return new Vector2Int(
      Mathf.RoundToInt((u.x - Dx) / Sx),
      Mathf.RoundToInt((u.y - Dy) / Sy));
  }
}

[RequireComponent(typeof(StrandColorRng))]
public class Beardscape : MonoBehaviour {
  public Ikabur.LD50_Beard.StrandBehaviour strandPrefab;

  public Texture2D beard;

  public int planePxStep = 8;

  public float planeSize = 5.0f;

  public float rndOffset = 0.35f;

  public float strandBend = 0.5f;

  public float growthTurbulence = 0.5f;

  public Ikabur.LD50_Beard.StrandOptions strandOptions;

  List<Ikabur.LD50_Beard.StrandBehaviour> strands;

  public List<Ikabur.LD50_Beard.StrandBehaviour> Strands { get { return strands; } }

  BeardscapeMap map;

  void Start() {
    CreateStrands();
    ResetStrands(1f);
  }

  void Update() {
    foreach (var s in strands) {
      s.growthTarget = GrowthTarget(s.Strand);
      s.GrowAndUpdate();
    }
  }

  void CreateStrands() {
    int w = beard.width;
    int h = beard.height;
    map = new BeardscapeMap(planeSize, w, h);

    var mesh = Ikabur.LD50_Beard.Strand.CreateMesh(strandOptions);

    strands = new List<Ikabur.LD50_Beard.StrandBehaviour>();

    var strandColorRng = GetComponent<StrandColorRng>();

    var cols = beard.GetPixels32();
    
    float rootPosRng = rndOffset * planeSize * planePxStep / (float)w;

    for (int i = planePxStep / 2; i < w; i += planePxStep) {
      for (int j = planePxStep / 2; j < h; j += planePxStep) {
        // check if strands are growing here
        var col = cols[j * w + i];
        if ((float)col.r - col.g - col.b < 128) { continue; }

        // position on beard plane
        Vector2 u = map.MapPixelToWorld(i, j);

        // add random offset
        u += rootPosRng * new Vector2(Random.Range(-1f, +1f), Random.Range(-1f, +1f));

        // create new strand
        var strand = GameObject.Instantiate(strandPrefab);
        strand.transform.parent = transform;
        strand.transform.position = Vector3.zero;
        strand.startRootPos = new Vector3(u.x, u.y, 0.0f) + transform.position;
        strand.mapPxPos = new Vector2Int(i, j);  // fixme should add rng offset

        // pick random color 
        var opts = strandOptions;
        opts.growthSpeed *= 1f + Random.Range(-0.05f, +0.05f);
        opts.width *= 1f + Random.Range(-0.05f, +0.05f);
        opts.springFactor *= 1f + Random.Range(-0.05f, +0.05f);
        opts.stiffness *= 1f + Random.Range(-0.05f, +0.05f);
        opts.dampenFactor *= 1f + Random.Range(-0.05f, +0.05f);
        strand.SetOptions(opts);
        strand.SetColor(strandColorRng.RandomColor());
        strand.SetMesh(mesh);

        strands.Add(strand);
      }
    }

    Debug.Log(string.Format("Generated {0} strands", strands.Count));
  }

  public void ResetStrands(float length) {
    foreach (var s in strands) {
      s.Strand.ResetLength(length);
    }
  }

  public Vector3 GrowthTarget(Ikabur.LD50_Beard.Strand strand) {
    // initially grow forward
    // the longer the strand the more grow down
    float w = strand.TotalLength;
    Vector3 n = Vector3.forward;
    Vector3 g = -Vector3.up;
    float mix = Mathf.Exp(-strandBend * w);
    return strand.Tip + 10.0f * (mix * n + (1.0f - mix) * g) + growthTurbulence * Random.insideUnitSphere;
  }
}
