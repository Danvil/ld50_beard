using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  public class LevelControl : MonoBehaviour {
    public RectTransform menu;
    public RectTransform report;

    public GameObject razor;

    public Beardscape beardscape;

    public StyleGuide styleGuide;
    public BeardMeasure measure;

    public Girlfriend girlfriend;

    public Level[] levels;

    int lastLevel;

    void Start() {
      MyOnInit();
    }

    void Update() {

    }

    public void MyOnInit() {
      menu.gameObject.SetActive(true);
      report.gameObject.SetActive(false);
      razor.SetActive(false);
      girlfriend.gameObject.SetActive(false);
    }

    public void MyOnWaitForSelection() {
      menu.gameObject.SetActive(false);
      report.gameObject.SetActive(true);
      razor.SetActive(false);
      girlfriend.gameObject.SetActive(false);
    }

    public void MyOnStartGame() {
      MySetLevel(0);
    }
    
    public void MyOnTryAgain() {
      MySetLevel(lastLevel);
    }

    public void MyOnNextLevel() {
      MySetLevel((lastLevel + 1) % levels.Length);
    }

    public void MySetLevel(int i) {
      lastLevel = i;

      var level = levels[i];

      beardscape.ResetStrands(1f);

      styleGuide.SetLevel(level);
      measure.SetLevel(level);

      girlfriend.ResetLove();
      girlfriend.gameObject.SetActive(true);

      menu.gameObject.SetActive(false);
      report.gameObject.SetActive(false);

      razor.SetActive(true);
    }
  }
}
