﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  [CreateAssetMenu(fileName = "BeardCutLevels", menuName = "Beard/New BeardCutLevels")]
  public class BeardCutLevels : ScriptableObject {
    public Color[] targetColors;
    public float[] targetLength;

    public Color[] levelCutColors;

    public float LevelColorToTargetLength(Color col) {
      return targetLength[FindBestMatchIndex(col, targetColors)];
    }

    public Color LevelColorToVisColor(Color col) {
      return levelCutColors[FindBestMatchIndex(col, targetColors)];
    }

    public static int FindBestMatchIndex(Color col, Color[] target) {
      Vector3 v = ColorToVec3(col);
      float best = 100f;
      int bestIndex = -1;
      for (int i = 0; i < target.Length; i++) {
        float dist = (v - ColorToVec3(target[i])).magnitude;
        if (dist < best) {
          best = dist;
          bestIndex = i;
        }
      }
      return bestIndex;
    }

    public static Vector3 ColorToVec3(Color col) {
      return new Vector3(col.r, col.g, col.b);
    }
  }
}
