﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrandColorRng : MonoBehaviour {
  public float strandColorVariation;

  public Color[] strandColors;
  public float[] strandColorWeights;

  public Color baseColor;
  public float[] luminosityChange;
  public float[] luminosityChangeWeights;

  float[] acc;

  public void Start() {
    acc = null;
  }

  public Color RandomColor() {
    return RandomLuminosityPick();
    //return RandomColorPick();
  }

  Color Variate(Color col) {
    col.r += strandColorVariation * Random.Range(-1f, +1f);
    col.g += strandColorVariation * Random.Range(-1f, +1f);
    col.b += strandColorVariation * Random.Range(-1f, +1f);
    return col;
  }

  Color RandomLuminosityPick() {
    float lum = luminosityChange[RandomWeightedIndex(luminosityChangeWeights)];
    float H, S, V;
    Color.RGBToHSV(baseColor, out H, out S, out V);
    return Variate(Color.HSVToRGB(H, S, V + lum));
  }

  Color RandomColorPick() {
    return Variate(strandColors[RandomWeightedIndex(strandColorWeights)]);
  }

  private int RandomWeightedIndex(float[] weights) {
    int n = weights.Length;
    if (acc == null || acc.Length != weights.Length) {
      acc = new float[n];
      for (int i = 0; i < n; i++) {
        acc[i] = (i == 0 ? 0f : acc[i - 1]) + weights[i];
      }
    }
    float q = Random.Range(0f, acc[n - 1]);
    for (int i = 0; i < n; i++) {
      if (q <= acc[i]) {
        return i;
      }
    }
    return n - 1;
  }
}
