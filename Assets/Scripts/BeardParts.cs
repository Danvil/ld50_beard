﻿using System;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  [CreateAssetMenu(fileName = "Parts", menuName = "Beard/New Parts", order = 1)]
  public class BeardParts : ScriptableObject {
    public Texture2D map;

    public Color[] partColors;

    public string[] partNames;

    public int PartCount { get { return partColors.Length; } }

    public int GetPartIndex(int i, int j) {
      var col = map.GetPixel(i, j);
      return BeardCutLevels.FindBestMatchIndex(col, partColors);
    }
  }
}
