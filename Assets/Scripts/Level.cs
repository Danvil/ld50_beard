﻿using System;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  [CreateAssetMenu(fileName = "Level", menuName = "Beard/New Level", order = 1)]
  public class Level : ScriptableObject {
    public String title;
    public String description;
    public Texture2D map;
  }
}
