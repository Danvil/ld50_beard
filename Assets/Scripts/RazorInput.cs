using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RazorInput : MonoBehaviour {
  public Transform tip;

  public RazorBlade blade;

  public Quaternion postRot = Quaternion.Euler(90f, 0f, 0f);
  public Vector3 postPos = Vector3.zero;

  public float maxSpeed = 3f;
  public float speedScale = 2f;

  public RazorCut cut;
  public TextMeshPro text;
  public float cutLevelScale = 0.1f;

  int cutLevel;

  public Vector3 GetPositionOffset() {
    return postPos + new Vector3(0f, 0f, cutLevel * cutLevelScale);
  }

  Vector3 current;

  Vector3 Step(Vector3 c, Vector3 t, float scl, float max, float dt) {
    Vector3 delta = t - c;
    float dist = delta.magnitude;
    if (dist < 0.01) { return t; }

    Vector3 v = delta * scl;
    float vm = dist * scl;
    if (vm > max) {
      v *= max / vm;
      vm = max;
    }

    Vector3 step = dt * v;
    float stepM = dt * vm;
    if (stepM > dist) {
      step *= dist / stepM;
    }

    return c + step;
  }

  void Start() {
    current = Vector3.zero;
    SetCutLevel(5);
  }

  void Update() {
    // Toggle razor on / off
    if (Input.GetKeyDown(KeyCode.R) || Input.GetMouseButtonDown(0)) {
      blade.Toggle();
    }

    // Razor level
    KeyCode[] cutLevelKeys = new KeyCode[] {
      KeyCode.Alpha1,
      KeyCode.Alpha2,
      KeyCode.Alpha3,
      KeyCode.Alpha4,
      KeyCode.Alpha5,
      KeyCode.Alpha6,
      KeyCode.Alpha7,
      KeyCode.Alpha8,
      KeyCode.Alpha9,
    };
    for (int i = 0; i < 9; i++) {
      if (Input.GetKeyDown(cutLevelKeys[i])) {
        SetCutLevel(i + 1);
      }
    }
    int msd = (int)Input.mouseScrollDelta.y;
    if (msd > 0) {
      SetCutLevel(cutLevel + 1);
    }
    if (msd < 0) {
      SetCutLevel(cutLevel - 1);
    }

    // Move razor with mouse
    Vector3 mouse = Input.mousePosition;
    mouse.z = 10f;
    Vector3 target = Camera.main.ScreenToWorldPoint(mouse);

    current = Step(current, target, speedScale, maxSpeed, Time.deltaTime);

    float angle = current.x * 10f * Mathf.Deg2Rad;
    var q = Quaternion.FromToRotation(new Vector3(-Mathf.Sin(angle), -Mathf.Cos(angle), 0f), -Vector3.right);

    transform.rotation = q * postRot;
    transform.position = current - q * tip.localPosition + GetPositionOffset();
  }

  void SetCutLevel(int level) {
    level = Mathf.Clamp(level, 1, 9);
    cutLevel = level;
    cut.cutLength = (level - 1) * cutLevelScale;
    text.text = level.ToString();
  }
}
