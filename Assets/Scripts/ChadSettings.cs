using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ChadSettings : MonoBehaviour
{
  public Color[] backgroundColors;
  public Color[] skinColors;
  public Color[] beardColors;

  public int leftPick;
  public int rightPick;
  public int skinPick;
  public int beardPick;

  public GameObject leftBckg;
  public GameObject rightBckg;
  public GameObject chad;
  public StrandColorRng strandColorRng;

  public void Update() {
    leftBckg.GetComponent<MeshRenderer>().sharedMaterial.SetColor("_BaseColor", backgroundColors[leftPick]);
    rightBckg.GetComponent<MeshRenderer>().sharedMaterial.SetColor("_BaseColor", backgroundColors[rightPick]);
    chad.GetComponent<MeshRenderer>().sharedMaterial.SetColor("_BaseColor", skinColors[skinPick]);
    strandColorRng.baseColor = beardColors[beardPick];
  }
}
