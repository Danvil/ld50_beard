﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Ikabur.LD50_Beard {
  public class Girlfriend : MonoBehaviour {
    public BeardMeasure beardMeasure;

    public RawImage heart;

    public float initialLove;

    public float loveTimeDecay;

    public float[] loveStarDecay;

    public RectTransform rtLove;
    public RectTransform rtAlone;
    public Button buttonNextLevel;
    public LevelControl ctrl;

    float love;

    public void ResetLove() {
      love = initialLove;
    }

    void Start() {
      ResetLove();
    }

    void Update() {
      love += loveTimeDecay * Time.deltaTime;

      var stars = beardMeasure.Stars;
      for (int i = 0; i < stars.Length; i++) {
        int star = Mathf.Clamp(stars[i], 1, 5) - 1;
        love += loveStarDecay[star] * Time.deltaTime;
      }

      heart.rectTransform.sizeDelta = new Vector2(192f, 192f * love / 100f);
      heart.uvRect = new Rect(0f, 0f, 1f, love / 100f);

      if (love > 100f) {
        GameOverLove();
      }

      if (love < 0f) {
        GameOverAlone();
      }
    }

    void GameOverLove() {
      rtLove.gameObject.SetActive(true);
      rtAlone.gameObject.SetActive(false);
      buttonNextLevel.enabled = true;
      ctrl.MyOnWaitForSelection();
    }

    void GameOverAlone() {
      rtLove.gameObject.SetActive(false);
      rtAlone.gameObject.SetActive(true);
      buttonNextLevel.enabled = false;
      ctrl.MyOnWaitForSelection();
    }
  }
}
