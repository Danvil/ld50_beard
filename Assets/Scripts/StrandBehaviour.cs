﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Ikabur.LD50_Beard {
  [RequireComponent(typeof(MeshFilter), typeof(SkinnedMeshRenderer))]
  public class StrandBehaviour : MonoBehaviour {
    public Vector3 growthTarget;

    public StrandOptions options;

    public Vector2Int mapPxPos;
    public Vector3 startRootPos;

    public int physicsSteps = 4;
    public float maxDt = 0.10f;

    public bool isGrowing = true;

    public float cutCloneCooldown = 1f;
    float cutCloneLast;

    Strand strand;

    Transform[] bones;

    SkinnedMeshRenderer smr;
    SkinnedMeshRenderer Renderer {
      get {
        if (smr == null) {
          smr = GetComponent<SkinnedMeshRenderer>();
        }
        return smr;
      }
    }

    public Strand Strand {
      get { return strand; }
      set { strand = value; }
    }

    public void RecoverBonesFromTransform() {
      bones = new Transform[strand.SegmentCount];
      for (int i = 0; i < strand.SegmentCount; i++) {
        var parent = i == 0 ? transform : bones[i - 1];
        bones[i] = parent.transform.GetChild(0);
      }
    }

    public void SetOptions(StrandOptions options) {
      this.options = options;
    }

    public void SetMesh(Mesh mesh) {
      GetComponent<MeshFilter>().sharedMesh = mesh;
      Renderer.sharedMesh = mesh;
    }

    public void Start() {
      cutCloneLast = Time.time;

      if (strand == null) {
        strand = new Strand(options);
      }

      if (isGrowing) {
        strand.StartGrowth(startRootPos);
      }

      if (bones == null) {
        bones = new Transform[strand.SegmentCount];
        for (int i = 0; i < strand.SegmentCount; i++) {
          var go = new GameObject("bone_" + i.ToString());
          var t = go.transform;
          t.parent = (i == 0) ? transform : bones[i - 1];
          bones[i] = t;
        }
      }

      UpdateBones();
      Renderer.bones = bones;
    }

    public void GrowAndUpdate() {
      float dt = MathF.Min(Time.deltaTime, maxDt);

      if (isGrowing) {
        strand.GrowTowards(dt, growthTarget);
      }

      strand.ApplyPhysics(dt, physicsSteps);

      UpdateBones();
    }

    private void UpdateBones() {
      var bonePoses = strand.UpdateAndGetBones();
      //Debug.Assert(bonePoses.Length == bones.Length, String.Format("{0} vs {1}", bonePoses.Length, bones.Length));
      for (int i = 0; i < bones.Length; i++) {
        var t = bones[i];
        t.localPosition = bonePoses[i].p;
        t.localRotation = bonePoses[i].q;
        t.localScale = bonePoses[i].s;
      }
    }

    public void SetColor(Color color) {
      Renderer.material.SetColor("_BaseColor", color);
    }

    public Strand Cut(float length) {
      Strand clone = strand.Cut(length);
      if (cutCloneLast + cutCloneCooldown > Time.time) {
        return null;
      } else { 
        cutCloneLast = Time.time;
        return clone;
      }
    }
  }
}
